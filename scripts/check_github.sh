#!/usr/bin/env bash

args=$(
cat <<EOF
-fsSL -H "Accept: application/json" -H "PRIVATE-TOKEN: ${SETTINGS__GITHUB_ACCESS_TOKEN}"
EOF
)

t=$(mktemp)
IFS=$'\n'
for i in $(cat $1); do
  n=$(echo ${i} | awk -F':' '{print $1}')
  v=$(curl -fsSL \
           -H "Accept: application/json" \
           -H "PRIVATE-TOKEN: ${SETTINGS__GITHUB_ACCESS_TOKEN}" \
           "https://api.github.com/repos/${n}/releases/latest" | \
      jq -r '.tag_name')
  echo "${n}: ${v}" >> ${t}
done
cat ${t} | sort | uniq | sed '/^$/d'
